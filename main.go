package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"todoapp/model"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var json map[string]interface{} = map[string]interface{}{}

func main() {

	type Todos struct {
		Id      string `json:"id"`
		Title   string `json:"title"`
		Content string `json: "content"`
	}

	db, err := sql.Open("mysql", "root:Y@kup.,58@tcp(localhost:3306)/tododb")
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("db is connected")
	}
	defer db.Close()
	// make sure connection is available
	err = db.Ping()
	if err != nil {
		fmt.Println(err.Error())
	}

	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))

	e.POST("/todo", func(c echo.Context) error {
		todo := new(model.Todo)
		if err := c.Bind(todo); err != nil {
			return err
		}
		pros, _ := db.Prepare("INSERT INTO `tododb`.`todo` (`t_title`, `t-content`) VALUES (?, ?);")
		data, _ := pros.Exec(todo.Newtitle, todo.Newcontent)
		lastId, _ := data.LastInsertId()
		fmt.Println("Son eklenen veri id: ", lastId)

		fmt.Println(todo)

		return c.JSON(http.StatusCreated, todo)
	})

	e.GET("/todo", func(c echo.Context) error {

		var id string
		var title string
		var content string

		var allTodo []Todos

		res, err := db.Query("SELECT * FROM todo")
		defer res.Close()
		if err != nil {
			fmt.Println(err)
		}

		for res.Next() {

			err := res.Scan(&id, &title, &content)

			if err != nil {
				fmt.Println(err)
			}

			allTodo = append(allTodo, Todos{Id: id, Title: title, Content: content})
		}

		return c.JSON(http.StatusOK, allTodo)

	})
	e.Start(":5000")
}
