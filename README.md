# ToDo-Backend

**Kullanılan teknolojiler**

- Golang echo
- MySql

**Özellikleri**
ToDo listeleme

**Kurmak için:**

- go mod init sql
- go get github.com/go-sql-driver/mysql
- go run main.go
